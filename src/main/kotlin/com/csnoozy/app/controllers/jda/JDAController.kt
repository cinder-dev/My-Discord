package com.csnoozy.app.controllers.jda

import com.csnoozy.app.controllers.PrimaryController
import com.github.thomasnield.rxkotlinfx.toObservable
import io.reactivex.subjects.PublishSubject
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.entities.Game
import tornadofx.*

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/28/2018
*
* File: com.csnoozy.app.controllers.jda.JDAController
* Description:
*/

class JDAController : Controller() {

    private val primaryController : PrimaryController by inject()

    val jda : JDA

    val jdaStatus = PublishSubject.create<String>()

    init {
        jda = JDABuilder(AccountType.CLIENT).apply {
            setToken(primaryController.token)
            setGame(Game.playing("with the discord API"))
            addEventListener(JDAListener(this@JDAController))
        }.buildAsync()

        primaryController.applicationRunning.subscribe {
            println("applicationRunning state changed to $it")
            if (!it) {
                jda.shutdownNow()
            }
        }

        runAsync {
            while (jda.status != JDA.Status.CONNECTED) {
                jda.status.name.toProperty().toObservable().subscribe(jdaStatus)
            }
            jda.status.name.toProperty().toObservable().subscribe(jdaStatus)
            println("Run End")

        }
    }
}