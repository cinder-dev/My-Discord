package com.csnoozy.app.views

import com.csnoozy.app.controllers.PrimaryController
import com.github.thomasnield.rxkotlinfx.actionEvents
import com.github.thomasnield.rxkotlinfx.toBinding
import com.github.thomasnield.rxkotlinfx.toObservable
import tornadofx.*

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/28/2018
*
* File: com.csnoozy.app.views.TokenAuthView
* Description:
*/

class TokenAuthView : View("My Discord") {

    private val controller : PrimaryController by inject()

    override val root = borderpane {
        top {
            vbox {
                text("Please enter your discord token.")
                text("This is now a Test!")
            }
        }
        center {
            hbox {
                label("Token: ")
                val input = textfield().apply {
                    textProperty().toObservable().subscribe(controller.tokenField)
                }
                button("Submit").apply {
                    disableProperty().bind(input.textProperty().toObservable().map { it.length != 59 }.toBinding())
                    actionEvents().subscribe(controller.submitToken)
                }
            }
        }
        bottom {
            text("You can find your token by pressing: 'Ctrl+Shift+I' in the discord app, under Application > Local Storage > https://discordapp.com > token.")
        }
    }

    init {
        controller.jdaLoad.subscribe {
            if (it) {
                replaceWith<JDALoadingView>()
            }
        }
    }
}