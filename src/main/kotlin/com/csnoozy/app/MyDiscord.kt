package com.csnoozy.app

import com.csnoozy.app.controllers.PrimaryController
import com.csnoozy.app.views.TokenAuthView
import com.github.thomasnield.rxkotlinfx.toObservable
import io.reactivex.Single
import tornadofx.*

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/23/2018
*
* File: com.csnoozy.app.MyDiscord
* Description:
*
* Token: MTA2Nzg0NDYwMjcxMjY3ODQw.DWjhlg.-lficIcvTaVSzYJBrr3-g9yDg2Q
*/

class MyDiscord : App(TokenAuthView::class){

    private val controller: PrimaryController by inject()

    override fun stop() {
        println("Application Shutting down")
        false.toProperty().toObservable().subscribe(controller.applicationRunning)
    }
}